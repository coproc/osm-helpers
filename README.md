# OSM helpers

Simple [Python](https://www.python.org) interfaces for some services around [OpenStreetMap](https://www.openstreetmap.org)

## Installation
Copy `geo_coords.py` to your project.

## Geocoding of Addresses

Usage:

```python
import geo_coords

locations = geo_coords.searchAddress('Stadtplatz', 27, city='Steyr')
print('%d locations found for address "Stadtplatz 27, Steyr"' % len(locations))
for loc in locations:
	print('  %s' % loc['properties']['display_name'])

bbox = geo_coords.getBoundingBox(locations)
print('\nbounding box of locations:')
print('  center longitude: %.6f' % bbox.center.lng)
print('  center latitude : %.6f' % bbox.center.lat)
print('  radius: %.1f m' % bbox.radius_m)
```

output:

```
4 locations found for address "Stadtplatz 27, Steyr":
  Rathaus, 27, Stadtplatz, Zwischenbrücken, Innere Stadt, Steyr, Oberösterreich, ...
  Fremdenverkehrsverband Steyr, 27, Stadtplatz, Zwischenbrücken, Innere Stadt, Steyr, ...
  ...

bounding box of locations:
  center longitude: 14.419811
  center latitude : 48.038788
  radius: 39.9 m
```
