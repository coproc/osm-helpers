'''
auxiliary functions aroung geo-coords
'''

import collections
import enum
import math

class BBoxIndex(enum.IntEnum):
	LNG_MIN = 0
	LAT_MIN = 1
	LNG_MAX = 2
	LAT_MAX = 3

GeoPoint = collections.namedtuple('GeoPoint', ['lng', 'lat'])
BBox = collections.namedtuple('BBox', ['min','max','center','radius_m'])

# haversine function (half of versine of an angle given in radians)
def _hav(angle_rad):
	s = math.sin(angle_rad/2)
	return s*s

# inverse haversine function
def _hav_inv_rad(hav):
	# the haversine value hav must be in the interval [0,1],
	# but could be outside because of rounding errors.
	hav_01 = min(max(hav, 0), 1)
	return 2*math.asin(math.sqrt(hav_01))

# compute great circle distance on unit sphere between two GeoPoint-s
# see: https://en.wikipedia.org/wiki/Haversine_formula
def _getHaversineDist_rad(p1, p2):
	lng1_rad, lat1_rad = [math.radians(c) for c in p1]
	lng2_rad, lat2_rad = [math.radians(c) for c in p2]
	hav_dist = (_hav(lat2_rad - lat1_rad) +
		math.cos(lat1_rad)*math.cos(lat2_rad)*_hav(lng2_rad - lng1_rad))
	return _hav_inv_rad(hav_dist)


# earth radius according to WGS84 reference system
# see: https://de.wikipedia.org/wiki/World_Geodetic_System_1984
EARTH_RADIUS_MAX_KM = 6378.137
EARTH_RADIUS_MIN_KM = 6356.752
EARTH_RADIUS_AVG_KM = (2*EARTH_RADIUS_MAX_KM + EARTH_RADIUS_MIN_KM)/3


# compute the direct distance of two points on planet Earth
def getCoordDist_m(p1, p2):
	try:
		# module geopy is not a standard module,
		# it can be used to compute very accurate direct distances.
		# see the documentation at https://geopy.readthedocs.io for how to install it
		import geopy.distance
		return 1000 * geopy.distance.distance((p1.lat, p1.lng), (p2.lat, p2.lng)).km
	except ImportError:
		# if module geopy is not installed we use the little bit less accurate haversine formula
		return 1000 * EARTH_RADIUS_AVG_KM * _getHaversineDist_rad(p1, p2)

def getBoundingBox(features):
	if len(features) == 0:
		return None
	boundingBoxes = [feature['bbox'] for feature in features]
	lngMin = min([bbox[BBoxIndex.LNG_MIN] for bbox in boundingBoxes])
	latMin = min([bbox[BBoxIndex.LAT_MIN] for bbox in boundingBoxes])
	lngMax = max([bbox[BBoxIndex.LNG_MAX] for bbox in boundingBoxes])
	latMax = max([bbox[BBoxIndex.LAT_MAX] for bbox in boundingBoxes])
	bbMin  = GeoPoint(lngMin, latMin)
	bbMax  = GeoPoint(lngMax, latMax)
	center = GeoPoint((lngMin+lngMax)/2, (latMin+latMax)/2)
	radius_m = max([getCoordDist_m(center, bbMin), getCoordDist_m(center, bbMax)])
	return BBox(bbMin, bbMax, center, radius_m)


#####################################################################
# code for testing
#####################################################################

def _test_getBoundingBox():
	import osm_geocoding as geocoding
	locations = geocoding.searchAddress('Stadtplatz', 27, city='Steyr')
	bbox = getBoundingBox(locations)
	print('coordinates for Stadtplatz 27:')
	print('  longitude: %.6f' % bbox.center.lng)
	print('  latitude : %.6f' % bbox.center.lat)
	print('  accuracy : +/-%.1f m' % bbox.radius_m)

def _test():
	_test_getBoundingBox()

if __name__ == '__main__':
	_test()
