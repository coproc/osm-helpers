'''
simple interface to the Nominatim service of OpenStreetMap.
get longitude and latitude of an address.

see the test functions at the end for usage examples.
'''

import requests

# sample nominatim request
# getting the location of the city hall ('Rathaus') of Steyr at Stadtplatz 27:
#   https://nominatim.openstreetmap.org/search?country=at&city=steyr&street=stadtplatz 27&format=json&addressdetails=1

# nominatim constants we use
LOCATION_SEARCH_URL = 'https://nominatim.openstreetmap.org/search'
SEARCH_PARAMETERS = {'format': 'geojson', 'addressdetails': '1'}


class GeocodingError(RuntimeError):
	pass


class ServiceConnectionError(GeocodingError):
	def __init__(self, reason):
		super().__init__("cannot connect to geocoding service: %s" % reason)
		self.reason = reason


class HttpStatusError(GeocodingError):
	def __init__(self, url, statusCode, reason):
		super().__init__("unexpected status %s (%d) for search request '%s'" %\
			(reason, statusCode, url))
		self.url = url
		self.statusCode = statusCode
		self.reason = reason


# add keys to address values as used by Nominatim
# see: https://nominatim.org/release-docs/develop/api/Search/#parameters
def _getAddressParameters(addressStreet, postCode, city, countryCode):
	addressParams = {'street': addressStreet, 'country': countryCode}
	if postCode is not None:
		addressParams['postalcode'] = str(postCode)
	if city is not None:
		addressParams['city'] = city
	return addressParams


def _searchAddress(addressStreet, postCode, city, countryCode):
	addressParams = _getAddressParameters(addressStreet, postCode, city, countryCode)
	searchParams = SEARCH_PARAMETERS.copy()
	searchParams.update(addressParams)
	try:
		response = requests.get(LOCATION_SEARCH_URL, params=searchParams)
	except requests.exceptions.ConnectionError as e:
		raise ServiceConnectionError(str(e))
	if response.status_code != requests.codes.ok:
		raise HttpStatusError(response.url, response.status_code, response.reason)
	return response.json()['features']


# get possible locations of given address
# note: at least one of the parameters 'postCode' 'and 'city' must be specified
# @return list of geojson features
def searchAddress(streetName, houseNr=None, postCode=None, city=None, countryCode='at'):
	if postCode is None and city is None:
		raise ValueError("at least one of the parameters 'postCode' 'and 'city' must be specified")
	addressStreet = streetName.strip()
	if houseNr is not None:
		addressStreet += ' ' + str(houseNr)

	return _searchAddress(addressStreet, postCode, city, countryCode)



#####################################################################
# code for testing
#####################################################################

def _test_searchAddress():
	locations = searchAddress('Stadtplatz', 27, city='Steyr')
	print('locations for Stadtplatz 27:')
	for loc in locations:
		print('  %s' % loc['properties']['display_name'])

def _test():
	_test_searchAddress()

if __name__ == '__main__':
	_test()
